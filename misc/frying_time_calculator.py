# ########################################################################################
#  Copyright (c) 2024. Viacheslav Kolupaev, author's website address:
#
#    https://vkolupaev.com/?utm_source=c&utm_medium=link&utm_campaign=notebook
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
#  file except in compliance with the License. You may obtain a copy of the License at
#
#    https://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed under
#  the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
#  ANY KIND, either express or implied. See the License for the specific language
#  governing permissions and limitations under the License.
# ########################################################################################

"""The module contains one function for solving a training task.

Call a function according to its signature; argument types must be respected.
"""

# Standard Library
from typing import Final


def calculate_total_frying_time(
    amount_of_cutlets_for_frying: int,
    frying_pan_capacity: int,
    cooking_time_for_one_side_seconds: int,
) -> int:
    """Calculate the total time for frying the cutlets.

    Args:
        amount_of_cutlets_for_frying: the number of cutlets that need to be fried.
        frying_pan_capacity: pan capacity in cutlets.
        cooking_time_for_one_side_seconds: frying time for one side of the cutlet in seconds.

    Raises:
        ValueError: if the function argument values are invalid.

    Returns:
        The number of seconds required to fry all the cutlets on both sides.

    """
    # Sanity check.
    if amount_of_cutlets_for_frying == 0:
        return 0
    elif amount_of_cutlets_for_frying < 0:
        raise ValueError('The number of cutlets for frying cannot be < 0.')

    if frying_pan_capacity < 1:
        raise ValueError('The capacity of the frying pan cannot be less than 1 cutlet.')

    if cooking_time_for_one_side_seconds <= 0:
        raise ValueError('The frying time of one side cannot be <= 0.')

    # Дано бизнес-условие: каждая котлета должна быть прожарена с двух сторон.
    number_of_sides_for_frying: Final[int] = 2

    # Если емкость сковороды в котлетах равна или превышает количество котлет к прожарке,
    # то сразу возвращаем результат — время прожарки с двух сторон по очереди.
    # Девять женщин не родят младенца за один месяц:-) Этот кейс обнаружил при
    # тестировании базового решения.
    if frying_pan_capacity >= amount_of_cutlets_for_frying:
        return cooking_time_for_one_side_seconds * number_of_sides_for_frying

    # Если же емкость сковороды в котлетах меньше количества котлет к прожарке, вычисляем
    # общее количество сторон к прожарке.
    total_number_of_sides_to_fry = (
        amount_of_cutlets_for_frying * number_of_sides_for_frying
    )

    # Далее жарим жадным способом: вычисляем число требуемых прожарок для сковород с
    # полной загрузкой и остаток от деления.
    frying_cycles_without_residue, residue = divmod(
        total_number_of_sides_to_fry,
        frying_pan_capacity,
    )

    # Вычисляем время, необходимое на прожарку всех сковород с полной загрузкой.
    total_frying_time_for_all_cutlets_seconds = (
        frying_cycles_without_residue * cooking_time_for_one_side_seconds
    )

    # Если был остаток — хотя бы 1 сторона котлеты, то потребуется добавить время
    # еще на один цикл жарки.
    if residue > 0:
        total_frying_time_for_all_cutlets_seconds += cooking_time_for_one_side_seconds

    return total_frying_time_for_all_cutlets_seconds


if __name__ == '__main__':
    # Рассчитываем и печатаем возможные результаты.
    min_frying_pan_capacity = 1
    max_frying_pan_capacity = 100
    possible_results: dict[int, int] = {}

    for frying_pan_capacity in range(min_frying_pan_capacity, max_frying_pan_capacity + 1):
        possible_results.update(
            {
                frying_pan_capacity: calculate_total_frying_time(
                    amount_of_cutlets_for_frying=100,
                    frying_pan_capacity=frying_pan_capacity,
                    cooking_time_for_one_side_seconds=(5 * 60),
                ),
            },
        )

    print(possible_results)
