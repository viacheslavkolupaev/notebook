# ########################################################################################
#  Copyright (c) 2023. Viacheslav Kolupaev, author's website address:
#
#    https://vkolupaev.com/?utm_source=c&utm_medium=link&utm_campaign=notebook
#
#  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
#  file except in compliance with the License. You may obtain a copy of the License at
#
#    https://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software distributed under
#  the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF
#  ANY KIND, either express or implied. See the License for the specific language
#  governing permissions and limitations under the License.
# ########################################################################################

import asyncio


async def execute_some_logic() -> int:
    await asyncio.sleep(1)
    return 222


# loop = asyncio.new_event_loop()
# task = loop.create_task(coro=execute_some_logic())
#
# try:
#     module_two_execution_result = loop.run_until_complete(task)
# finally:
#     loop.run_until_complete(loop.shutdown_asyncgens())
#     loop.close()

module_two_execution_result = asyncio.run(execute_some_logic())
